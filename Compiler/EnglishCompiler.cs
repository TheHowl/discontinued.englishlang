﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    class EnglishCompiler
    {
        static void Main(string[] args)
        {
            Regexes rgx = new Regexes();
            Console.WriteLine("What should I parse into various sentences?");
            string wsipiv = Console.ReadLine();
            List<string> sentences = rgx.getsentences(wsipiv);
            foreach (string sentence in sentences)
            {
                Console.WriteLine("match: " + sentence);
            }
            Console.WriteLine("Press a key to continue...");
            Console.ReadKey();
        }
    }
}
