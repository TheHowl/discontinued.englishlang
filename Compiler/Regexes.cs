﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


// http://www.dotnetperls.com/regex-match
namespace Compiler
{
    class Regexes
    {
        public List<string> getsentences(string str) {
            MatchCollection matches = Regex.Matches(str, @"([^.]+)[ .]+");
            List<string> matcheslist = new List<string>();
            foreach (Match match in matches)
            {
                matcheslist.Add(match.Groups[1].Value);
            }
            return matcheslist;
        }
    }
}
